package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    ArrayList<FridgeItem> fridgeArray = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        int itemsInFridge = 0;
        for(int i = 0; i < fridgeArray.size(); i++ ){
            itemsInFridge ++;
        }
        return itemsInFridge;
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        int max_size = 20;
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (fridgeArray.size()<totalSize()){
            fridgeArray.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) throws NoSuchElementException{
        // TODO Auto-generated method stub
        if (fridgeArray.contains(item)){
            fridgeArray.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        int tmp = fridgeArray.size();
        for(int i = 0; i < tmp; i++){
            fridgeArray.remove(0);

        }
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList<FridgeItem> expiredFoodArray = new ArrayList<FridgeItem>();
        for(int i = 0; i < nItemsInFridge(); i++){
            FridgeItem item = fridgeArray.get(i);
            if (item.hasExpired()){
                expiredFoodArray.add(item);
            }
        }
        for(int i = 0; i < expiredFoodArray.size(); i++){
            FridgeItem expiredItem = expiredFoodArray.get(i);
            fridgeArray.remove(expiredItem);
        }

        return expiredFoodArray;
    }

}
